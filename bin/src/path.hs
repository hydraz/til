{-# LANGUAGE LambdaCase #-}
module Main where

import qualified Data.Map.Strict as S

import Control.Concurrent.Async
import Control.Applicative

import System.Environment
import System.Directory

import Control.Arrow
import Control.Monad

import Data.List
import Data.Ord

executables :: IO [[String]]
executables = do
  x <- components <$> getEnv "PATH"
  forConcurrently x $ \p -> do
    e <- doesDirectoryExist p
    if e
       then listDirectory p
       else pure []

components :: String -> [String]
components = go [] where
  go acc (':':ys) = reverse acc:go [] ys
  go acc (x:xs) = go (x:acc) xs
  go acc _ = [reverse acc]

freqMap :: IO (S.Map String Int)
freqMap = realMap <|> pure (S.empty) where
  realMap = do
    doesFileExist "/home/hydraz/.cache/path_frequency" >>= guard
    S.fromAscList . map (head &&& length) . groupBy (==) . filter (/="") . sort . lines
      <$> readFile "/home/hydraz/.cache/path_frequency"

main :: IO ()
main = do
  xs <- filter ((/='.').head) . concat <$> executables
  fq <- freqMap
  let mkFreq x
        | x `S.member` fq = (fq S.! x, x)
        | otherwise = (0, x)
      freqs = map mkFreq xs
  mapM_ (putStrLn . snd) . sortOn (Down . fst) $ freqs
