{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE OverloadedStrings #-}
module Main where

import System.Process
import System.IO
import System.Exit
import System.Environment
import System.IO.Unsafe -- forgive me lord, for I have sinned.

import Text.Printf

import qualified Data.ByteString.Char8 as B

slice :: Int -> Int -> B.ByteString -> B.ByteString
slice from to xs = B.take (to - from + 1) (B.drop from xs)

data Status = Untracked | Change | Stage | None deriving (Eq, Show, Ord)

parse :: B.ByteString -> Status
parse xs
  | B.null xs = None 
  | otherwise = if | x == 'A'     -> Stage
                   | y == 'M'     -> Change
                   | y == 'D'     -> Change
                   | mode == "??" -> Untracked
                   | otherwise    -> None
                   where mode        = slice 0 1 xs
                         Just (x, ys) = B.uncons mode
                         Just (y, _)  = B.uncons ys

main :: IO ()
main = do 
  x <- lookupEnv "DO_GIT_PROMPT"
  case x of
    Nothing -> pure ()
    Just y -> case y of
                "no" -> exitSuccess
                "0" -> exitSuccess
                _ -> pure ()
  (status, branch', _) <- readProcessWithExitCode "git" ["symbolic-ref", "HEAD"] ""
  case status of
    ExitSuccess     -> pure ()
    e@ExitFailure{} -> exitWith e
  let branch = drop 11 $ init branch'
  args <- getArgs
  let fmt = case args of
             ["plain"] -> "%s\n%d\n%d\n%d"
             [x] -> x
             []  -> "%s:%d:%d:%d"
  case branch of
    [] -> exitSuccess
    _ ->  do (_, Just hout, _, _) <- createProcess (proc "git" ["status", "--porcelain"]) { std_out = CreatePipe }
             contents <- B.hGetContents hout
             let changeset = map parse (B.lines contents)
                 stages    = length $ filter (== Stage)     changeset
                 changes   = length $ filter (== Change)    changeset
                 untracked = length $ filter (== Untracked) changeset
              in printf fmt branch changes stages untracked
             exitSuccess

