# Set up nix overlays

target=$1; shift

overlay () {
  repo=$1; name=$2; shift; shift; files=$@;
  repo_path="$target/.config/nixpkgs/.overlays/$name"
  if [[ -d "$repo_path" ]]; then
    pushd $repo_path &>/dev/null
      git pull
    popd &>/dev/null
  else
    git clone -q --recursive --depth 1 "$repo" $repo_path
  fi
  echo "$(tput setaf 2)OVER$(tput sgr0) $2"
  for f in $@; do
    mkdir -p $(dirname "$PWD/$f")
    ln -sf "$repo_path/$f" $target/.config/nixpkgs/overlays/$name-$f
  done
}

if [[ ! -d $target/.config/nixpkgs/.overlays ]]; then
  mkdir -p $target/.config/nixpkgs/.overlays
fi

overlay "https://github.com/mozilla/nixpkgs-mozilla" mozilla \
  "rust-overlay.nix" \
  "firefox-overlay.nix"
