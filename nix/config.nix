with import <nixpkgs> {};

let
  mkBinutils = target: binutils.override {
    targetPlatform = {
     config = target;
    };
    noSysDirs = 1;
  };
in
  {
    # Allow unfree packages. This is a per-user setting, and the value
    # specified in /etc/nixos/configuration.nix is only valid for the
    # system profile (root's nix-env).
    allowUnfree = true;

    # Override packages to provide better alternatives.
    packageOverrides = pkgs_: with pkgs_; {
      mumble = mumble.override {
        pulseSupport = true;
      };

      ncmpcpp = ncmpcpp.override {
        visualizerSupport = true;
      };

      ghc = with haskell.packages.ghc821;
        ghcWithPackages (pkgs: [ cabal-install pandoc pretty-show hscolour ]);

      attic = {
        rxvt-unicode = callPackage ./packages/urxvt/wrapper.nix {
          rxvt_unicode = callPackage ./packages/urxvt {};
          plugins = [];
        };
      };

      termbox-git = callPackage ./packages/termbox {};
      nvim-git = callPackage ./packages/neovim.nix {};
      ccemux = callPackage ./packages/ccemux.nix {};
      colorpicker = callPackage ./packages/colorpicker.nix {};
      syncplay = callPackage ./packages/syncplay.nix {};
      alacritty = callPackage ./packages/alacritty.nix {};
      nyancat = callPackage ./packages/nyancat.nix {};
      shoot = callPackage ./packages/shoot {};

      binutils_i386 = mkBinutils "i386-unknown-linux-gnu";
      binutils_x86_64 = mkBinutils "x86_64-unknown-linux-gnu";

      tpt = the-powder-toy.overrideAttrs (old: rec {
        version = "92.1.332";
        name = "tpt-${version}";
        src = fetchFromGitHub {
          owner = "simtr";
          repo = "The-Powder-Toy";
          rev = "v${version}";
          sha256 = "0gfx8qvyi0sif6pgqn7y9gn633zwfhzhfw63dksi9cvwbxjzkl62";
        };
      });

      polybar = (polybar.overrideAttrs (old: rec {
        src = fetchgit {
          url = "https://github.com/jaagr/polybar.git";
          branchName = "e329a8150ace9c8d5eac35771ae49ef3f73a1cae";
          sha256 = "0b4f1hxqqc1nrahsh6isi1bnf7fv7kbi4l4c8slcr8cvm2c4dqy5";
        };
        version = "3.0.6";
        name = "polybar-${version}";
        patches = [];
      })).override {
        i3GapsSupport = true;
        mpdSupport = true;

        inherit (pkgs) i3-gaps mpd_clientlib;
      };

      haskell = haskell // {
        packages = haskell.packages // {
          ghc821-profiling = haskell.packages.ghc821.override {
            overrides = self: super: {
              mkDerivation = args: super.mkDerivation (args // {
                enableLibraryProfiling = true;
              });
            };
          };
        };
      };

      # This is getting out of hand.
      languages = buildEnv {
        name = "proglangs";
        paths = [ bison flex
                  llvmPackages_4.clang llvmPackages_4.lld
                  clisp
                  fsharp luajit
                  gcc7 gdb
                  haskellPackages.ghcid
                  guile haskellPackages.idris isabelle
                  lean mono nasm ocaml racket redprl sbcl
                  swiProlog z3 ];
      };
    };
  }
