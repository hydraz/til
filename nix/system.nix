{ config, pkgs, ... }:

{
  nixpkgs.config = import ./config.nix;
  imports = [ /etc/nixos/hardware-configuration.nix ];

  boot = {
    loader = { 
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };
    blacklistedKernelModules = [ "nouveau" ];
  };

  powerManagement.cpuFreqGovernor = null;

  networking = {
    hostName = "antero-vipunen";
    extraResolvconfConf = ''
    name_servers="8.8.8.8 8.8.4.4"
    '';
    networkmanager.enable = true;
  };

  i18n = {
    consoleKeyMap = "br-abnt2";
    defaultLocale = "en_GB.UTF-8";
  };

  time.timeZone = "America/Sao_Paulo";

  users.extraUsers.hydraz = {
    isNormalUser = true;
    uid = 1000;

    extraGroups = ["wheel" "networkmanager"];
    home = "/home/hydraz";
    shell = "/run/current-system/sw/bin/fish";
  };

  programs = {
    fish.enable = true;
  };

  hardware = {
    pulseaudio = {
      enable = true;
      support32Bit = true;
      package = pkgs.pulseaudioLight.override {
        x11Support = true;
      };
    };

    opengl = {
      driSupport32Bit = true;
    };
    cpu.intel.updateMicrocode = true;
  };

  services = {
    emacs.enable = true;
    xserver = {
      enable = true;
      layout = "br";
      windowManager = {
        i3 = {
          enable = true;
          package = pkgs.i3-gaps;
        };
        default = "i3";
      };
      displayManager.lightdm = {
        enable = true;
      };
      xkbOptions = "terminate:ctrl_alt_bksp, compose:menu";
      xkbVariant = "nodeadkeys";

      exportConfiguration = true;
      libinput = {
        enable = true;
        disableWhileTyping = true;
        naturalScrolling = true;
      };
    };

    redshift = {
      enable = true;
      longitude = "-25";
      latitude = "-49";
      extraOptions = [ "-v" "-m randr" ];
      temperature = {
        day = 5600;
        night = 3400;
      };
    };
    logind.extraConfig = ''
      HandlePowerKey=suspend
      HandleLidSwitch=suspend
      HandleLidSwitchDocked=ignore
    '';

    tlp.enable = true;
  };

  security.sudo = {
    enable = true;
    wheelNeedsPassword = false;
    extraConfig = ''
    Defaults insults
    '';
  };

  nix = {
    buildCores = 8;
    maxJobs = 8;
  };

  # Systemd config
  systemd = {
    services.tlp.restartIfChanged = false;
    user.services = {
      urxvtd = {
        after = [ "network.target" "graphical.target" ];
        description = "RXvt-Unicode Daemon";
        wantedBy = [ "default.target" ];
        path = [ pkgs.attic.rxvt-unicode ];

        script = "exec urxvtd";
      };
      mpd = {
        after = [ "network.target" "sound.target" ];
        description = "Music player daemon";
        wantedBy = [ "default.target" ];
        path = [ pkgs.mpd pkgs.mpc_cli ];

        script = "exec mpd --no-daemon";
        preStop = "exec mpc pause";
      };

      dunst = {
        after = [ "network.target" "graphical.target" ];
        description = "Notification daemon";
        wantedBy = [ "default.target" ];
        path = [ pkgs.dunst ];

        script = "exec dunst";
      };
    };
  };

  fonts = {
    fonts = with pkgs; [ gohufont ];
  };
}
