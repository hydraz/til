{ stdenv
, fetchFromGitHub
, python2
, python2Packages
}:

stdenv.mkDerivation rec {
  name = "syncplay-${version}";
  version = "1.4.0";

  src = fetchFromGitHub {
    owner = "Syncplay";
    repo = "syncplay";
    rev = "v${version}";
    sha256 = "1sbb6zpw5b17jlid3mgzhcc5q1c1mp535hx57c40nbn2qjqgvijj";
  };

  buildInputs = with python2Packages;
    [ python2 pyside wrapPython twisted ];
  propagatedBuildInputs = buildInputs;
  pythonPath = [ python2Packages.pyside python2Packages.twisted ];
  postInstall = "wrapPythonPrograms";

  configurePhase = ''
  export PREFIX=$prefix
  '';

  meta = {

  };
}
