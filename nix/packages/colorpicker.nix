{ stdenv, fetchFromGitHub
, gtk2
, x11
, gnumake
, pkgconfig }:

stdenv.mkDerivation rec {
  name = "colorpicker-git";
  version = "git";
  buildInputs = [ gtk2 x11 gnumake pkgconfig ];
  src = fetchFromGitHub {
    owner = "Ancurio";
    repo = "colorpicker";
    rev = "2492a1c6a68a9f4659cab356b80b48ed9f6d1aec";
    sha256 = "16pzfz87kfwb4zjl5b1vqrq533m5fmrz8jvhgrz5anc7qpdddnld";
  };

  installPhase = ''
  install -Dm755 colorpicker $out/bin/colorpicker
  '';
}
