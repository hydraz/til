{ stdenv, fetchFromGitHub }:

stdenv.mkDerivation rec {
  version = "1.5.1";
  name = "nyancat-v${version}";

  src = fetchFromGitHub {
    owner = "klange";
    repo = "nyancat";
    rev = version;
    sha256 = "0mil6qhvzs156bxrdlj0wn391gig9zlpa3dpi3hphavamzznfmy9";
  };

  patchPhase = ''
  substituteInPlace Makefile \
    --replace /usr/ $out/
  '';
  preInstall = ''
  mkdir -p $out/bin $out/share/man/man1
  '';
}
