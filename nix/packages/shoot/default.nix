{ stdenv
, xorg
, xdotool
, imagemagick
, slop
, libnotify
}:

let inherit (xorg) xdpyinfo;
in stdenv.mkDerivation {
  name = "shoot";
  src = ./.;
  phases = [ "installPhase" ];
  installPhase = ''
  install -Dm755 $src/shoot $out/bin/shoot
  '';
}
