{ stdenv, fetchurl, python2, fetchgit }:

stdenv.mkDerivation rec {
  name = "termbox-${version}";
  version = "git-master";

  src = fetchgit {
    url = "https://github.com/nsf/termbox";
    sha256 = "1525z1paydnwjnzddsnnjn26prib6bq4dwkmr56byny74zis4xsb";
    rev = "ba27655fb76de3786141c19faf5fb78c9967aab0";
  };

  buildInputs = [ python2 ];

  configurePhase = ''
  ./waf configure --prefix=/
  '';

  buildPhase = ''
  ./waf
  '';

  installPhase = ''
  ./waf install --destdir=$out
  mkdir -p $out/lib/pkgconfig
  cat >$out/lib/pkgconfig/termbox.pc <<EOF
  Name: termbox
  Description: Library for writing text-based user interfaces
  Version: 1.0.0
  Cflags: -I$out/include
  Libs: -L$out/lib -ltermbox
  '';

  meta = with stdenv.lib; {
    description = "Library for writing text-based user interfaces";
    homepage = https://github.com/nsf/termbox;
    license = licenses.mit;
  };
}
