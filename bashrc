#unset PS1
green="\e[1;32m"
blue="\e[1;34m"
_prmpt="\e[38;2;124;105;178m>\e[38;2;153;130;216m>\e[38;2;224;123;217m=\e[0m"
_etcpr="\e[38;2;124;105;178m.\e[38;2;153;130;216m.\e[38;2;224;123;217m.\e[0m"

#prompt () {
#  echo -ne "${green}$(whoami) ${blue}$(pwd) $_prmpt "
#}
#PS2="\[${green}\]\u \[${blue}\]\W $_etcpr "

#export PROMPT_COMMAND=prompt
export PATH=$PATH

[ -f ~/.fzf.bash ] && source ~/.fzf.bash
