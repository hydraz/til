" vim:fdm=marker

" Plug {{{
call plug#begin('~/.config/nvim/plugged')
Plug 'lervag/vimtex'
Plug 'neomake/neomake'
Plug 'junegunn/fzf.vim'
Plug 'godlygeek/tabular'
Plug 'othree/eregex.vim'
Plug 'tpope/vim-vinegar'
Plug 'tpope/vim-surround'
Plug 'leanprover/lean.vim'
Plug 'joshdick/onedark.vim'
Plug 'vim-pandoc/vim-pandoc'
Plug 'idris-hackers/idris-vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-pandoc/vim-pandoc-syntax'
Plug 'Shirk/vim-gas', { 'for': 'gas' }
Plug 'junegunn/rainbow_parentheses.vim'
Plug 'jpalardy/vim-slime', { 'for': 'lisp' }
Plug 'junegunn/fzf', { 'do': './install --all' }
Plug 'ndmitchell/ghcid', { 'rtp': 'plugins/nvim' }
Plug 'sheerun/vim-polyglot', { 'commit': '8148255ef1' }
call plug#end()
" }}}

" Base {{{
if (has("termguicolors"))
   set termguicolors
endif
filetype plugin indent on
syntax on

" Settings {{{
set tw=72
set ts=2 sts=2 sw=2 et
set listchars=eol:¬,tab:»\ ,trail:~,extends:»,precedes:«
set showcmd
set noswapfile
set list
set nu rnu
set eol
set undofile undodir=$HOME/.config/nvim/undo
set undolevels=10000 undoreload=100000
set fillchars=fold:\ 
set fixeol
set cursorline 
set clipboard=unnamedplus
set tags=./.git/tags,./tags,tags,.tags,./.tags
set formatoptions+=cro
set mouse+=a sh=bash
set exrc
" }}}

set background=dark
colo onedark
let mapleader = "\<Space>"
let maplocalleader = ","
" }}}

" Keyboard {{{
" Normal {{{
nnoremap h <Nop>
nnoremap j <left>
nnoremap k <down>
nnoremap l <up>
nnoremap ç <right>

nnoremap <CR> :let @/ = ''<CR><down>
nnoremap / /\v
nnoremap ? ?\v
nnoremap Q <nop>

nnoremap <C-w>h <Nop>
nnoremap <C-w>j :wincmd h<CR>
nnoremap <C-w>k :wincmd j<CR>
nnoremap <C-w>l :wincmd k<CR>
nnoremap <C-w>ç :wincmd l<CR>

nnoremap <C-p> :FZF<CR>
nnoremap T <C-]>
nnoremap <F4> :!ctags
" }}}

" Visual {{{
vnoremap h <Nop>
vnoremap j <left>
vnoremap k <down>
vnoremap l <up>
vnoremap ç <right>
" }}}

" Normal - leader {{{
nnoremap <leader>w :w<CR>
nnoremap <leader>q :x<CR>
nnoremap <leader>W :wall<CR>
nnoremap <leader>Q :xall<CR>
nnoremap <leader>f gqG
nnoremap <leader>c :Neomake<CR>
nnoremap <leader>to :terminal fish<CR>

nnoremap <leader>sv :so $MYVIMRC<CR>
nnoremap <leader>sf :so %<CR>

nnoremap <leader>h :echo SyntaxItem()<CR>
nnoremap <leader>r :Urn<CR>
nnoremap <leader>b :Buffers<CR>
" }}}

" Visual - leader {{{
vnoremap <leader>ls :'<,'>!sort<CR>
vnoremap <leader>lu :'<,'>!uniq<CR>
vnoremap <leader>f  gq
" }}}

" Terminal {{{
tnoremap <Esc> <C-\><C-n>
" }}}

" Insert {{{
inoremap fj <Esc>
inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"
inoremap <C-g> <CR>
"iunmap <C-g>s
"iunmap <C-g>S
" }}}
" }}}

" Plugin configuration {{{
fu! AirlineAfter()
   let g:airline_left_sep = ''
   let g:airline_left_alt_sep = ''
   let g:airline_right_sep = ''
   let g:airline_right_alt_sep = ''
   let g:airline_symbols.branch = 'B'
   let g:airline_symbols.readonly = 'R'
   let g:airline_symbols.linenr = 'L'
   let g:airline#extensions#tabline#enabled = 1
   let g:airline#extensions#tabline#left_sep = ' '
   let g:airline#extensions#tabline#left_alt_sep = '|'
   let g:airline_mode_map = {
            \ '__' : '-',
            \ 'n'  : 'N',
            \ 'i'  : 'I',
            \ 'R'  : 'R',
            \ 'c'  : 'C',
            \ 'v'  : 'V',
            \ 'V'  : 'V',
            \ '' : 'V',
            \ 's'  : 'S',
            \ 'S'  : 'S',
            \ '' : 'S',
            \ }
   AirlineToggleWhitespace
endfunction

let g:pandoc#syntax#conceal#blacklist = ['titleblock']
let g:pandoc#syntax#codeblocks#embeds#langs = ["haskell", "lisp"]
let g:pandoc#modules#disabled = ["folding"]

let g:markdown_fenced_languages = ['html', 'python', 'haskell']
let g:html_prevent_copy = "fntd"
let g:html_number_lines = 0
let g:html_font         = ["Iosevka", "Iosevka Term"]

let g:slime_target = "neovim"
let b:slime_config = { 'jobid': 1 }

let g:ale_linters = {
   \ 'haskell': [],
   \ 'asm': [],
   \ }

let g:gutentags_ctags_file = '.tags'
let g:ghci_command = 'maybe-nix-shell . "cabal repl"'
let g:polyglot_disabled = [ 'latex' ]
let g:vimtex_view_method = 'zathura'

let g:ghci_command = 'maybe-nix-shell . "cabal new-repl"'
" }}}

" Autocommands {{{
augroup vimrc
   au!
   au  BufWritePost $MYVIMRC source $MYVIMRC
   au! User AirlineAfterInit call AirlineAfter()
augroup END

augroup ft
   au!
   au BufEnter *.s,*.S               set filetype=gas
   au BufEnter *.wa                  set filetype=waffle syntax=haskell
   au BufEnter *.md,*.lhs            set spell tw=72 fo+=t
   au BufEnter *.x                   set filetype=alex
   au BufEnter *.y                   set filetype=happy
   au BufEnter *.tex                 set tw=72 filetype=tex fo+=t
   au BufEnter *.v                   set filetype=coq
   au BufEnter *.pl                  set filetype=prolog
   au BufEnter *.lisp                set filetype=urn
augroup END
" }}}

" Functions {{{
function! SyntaxItem()
   return synIDattr(synID(line("."),col("."),1),"name")
endfunction

function! UpRegion(ln1, ln2)
   exec '!upsl ' . a:ln1 . ' ' .  a:ln2 . ' ' . expand('%:p')
endfunction

function! SplitTerm(cmd, height)
   exec 'bo ' . a:height . 'split'
   exec 'terminal ' . a:cmd
endfunction

function! UrnRepl(height, ...)
   let bnr = bufwinnr('Urn Interaction')
   let urn_flags = ''
   if isdirectory('./lib')
      let urn_flags = urn_flags . '-ilib'
   endif

   if bnr > 0
      exe bnr . 'wincmd w'
   else
      exec 'bo ' . a:height . 'split'
      exec 'terminal rlwrap urn ' . urn_flags
      file 'Urn Interaction'
      exec bufwinnr('Urn Interaction') . 'wincmd w'
   endif
   if a:0 > 2
   else
      startinsert
   endif
endfunction

command! -range=% Upload call UpRegion(<line1>,<line2>)
command! -nargs=1 -range=15 T call SplitTerm(<q-args>, <count>)
command! -nargs=* -range=15 Urn call UrnRepl(<count>, <q-args>)
command! -bang -nargs=* Find call fzf#vim#grep('rg --column --line-number --no-heading --fixed-strings --ignore-case --no-ignore --hidden --follow --glob "!.git/*" --color "always" '.shellescape(<q-args>), 1, <bang>0)
command! Buffers call fzf#run(fzf#wrap(
         \ {'source': map(range(1, bufnr('$')), 'bufname(v:val)')}))

cabbrev urn <c-r>=(getcmdtype()==':' && getcmdpos()==1 ? 'Urn' : 'urn')<CR>
" }}}

" Digraphs {{{

digraph ns 8345 " subscript n

" }}}

" Highlight groups {{{
if $TERM != "dumb"
   hi Normal                     ctermbg=none guibg=none
endif
hi SignifySignAdd             ctermbg=none ctermfg=6
hi SignifySignChange          ctermbg=none ctermfg=3
hi SignifySignDelete          ctermbg=none ctermfg=1
hi SignifySignChangeDelete    ctermbg=none ctermfg=5
hi SignifySignDeleteFirstLine ctermbg=none ctermfg=1
hi markdownItalic             cterm=italic gui=italic
hi markdownBold               cterm=bold gui=bold
hi Comment                    cterm=italic gui=italic
hi CursorLine                 ctermbg=none guibg=none
hi Folded                     ctermbg=none ctermfg=5
hi SignColumn                 ctermbg=none
hi link Operator              Keyword
hi link haskellForall         haskellDecl
hi haskellType                guifg=#61AFEF
hi link luaFunction           luaStatement
hi link gasSymbolRef          Constant
hi link htmlBold              markdownBold
hi link htmlItalic            markdownItalic
hi link lhsRBirdTrack         haskellOperator
hi link lhsBirdTrack          haskellOperator
hi link Include               Structure
hi link haskellImportKeywords Structure
" }}
