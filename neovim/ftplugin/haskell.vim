" Haskell-specific settings.

setlocal ts=2 sw=2 et
setlocal omnifunc=necoghc#omnifunc

nnoremap <leader>lr :call SplitTerm('ghci')<CR>
nnoremap <silent> <leader>tw :GhcModTypeInsert<CR>
nnoremap <silent> <leader>ts :GhcModSplitFunCase<CR>
nnoremap <silent> <leader>tq :GhcModType<CR>
nnoremap <silent> <leader>te :GhcModTypeClear<CR>

let g:haskellmode_completion_ghc = 1
setlocal omnifunc=necoghc#omnifunc
