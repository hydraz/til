" Haskell-specific settings.

setlocal ts=2 sw=2 et
setlocal omnifunc=necoghc#omnifunc

nnoremap <leader>lr :call SplitTerm('ghci')<CR>
