syntax clear

unlet! b:current_syntax
syntax include @haskell syntax/haskell.vim

syntax match lithsBirdRight "^>" contained
syntax match lithsBirdLeft "^<" contained

syntax region lithsSource matchgroup=lithsBirdRight start="^>" end="$" contains=lithsBirdRight,@haskell
syntax region lithsExample matchgroup=lithsBirdLeft start="^<" end="$" contains=lithsBirdLeft,@haskell

hi default link lithsBirdRight Comment
hi default link lithsBirdLeft Comment

let b:current_syntax = "literatehs"
