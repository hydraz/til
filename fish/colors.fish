set -U fish_color_autosuggestion 555
set -U fish_color_cancel -r
set -U fish_color_command magenta
set -U fish_color_comment brblack
set -U fish_color_cwd green
set -U fish_color_cwd_root red
set -U fish_color_end brmagenta
set -U fish_color_error brred
set -U fish_color_escape 'bryellow'  '--bold'
set -U fish_color_happy green
set -U fish_color_history_current --bold
set -U fish_color_host normal
set -U fish_color_hostname magenta
set -U fish_color_match --background=brblue
set -U fish_color_normal brwhite
set -U fish_color_operator cyan
set -U fish_color_param white
set -U fish_color_prompt '#6e5d9d'
set -U fish_color_quote green
set -U fish_color_redirection brmagenta
set -U fish_color_sad red
set -U fish_color_search_match bryellow
set -U fish_color_selection 'white'  '--bold'  '--background=brblack'
set -U fish_color_status red
set -U fish_color_timing blue
set -U fish_color_user yellow
set -U fish_color_username yellow
set -U fish_color_valid_path cyan
set -U fish_pager_color_completion white
set -U fish_pager_color_description 'B3A06D'  'yellow'
set -U fish_pager_color_prefix 'white'  '--bold'  '--underline'
set -U fish_pager_color_progress brred
