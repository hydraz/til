(import lua/os os)

(define +func+ (car arg))
(unless +func+
  (format 2 "nix-fish-deps: called without function name (arguments were {#arg})"))

(define *template*
  "with import <nixpkgs> {};
   let inherit (pkgs) %deps%;
   in stdenv.mkDerivation rec {
       name = \"%func%\";
       src = %file%;
       buildInputs = [ %deps% ];
       phases = [ \"installPhase\" ];
       installPhase = ''
       echo \"%path%\" > temp
       cat $src >> temp
       install -Dm644 temp $out
       '';
     }")

(let* [(x (io/open (.. +func+ ".fish")))]
  (unless x
    (format 2 "nix-fish-deps: the function {} does not exist" +func+))
  (let* [(dep-line (self x :read "*l"))
         (contents (self x :read "*a"))]
    (case (list (string/match dep-line "%# nix%-fish%-deps: (.*)"))
      [(?x)
       (let* [(dependencies (string/split x ", "))
              (drv (string/gsub *template* "%%([%w-]+)%%"
                                { :dep-args x
                                  :path (format nil "set -x PATH {@( )} \\$PATH"
                                                (map (cute .. "${" <> "}/bin")
                                                     dependencies))
                                  :deps (concat dependencies " ")
                                  :func (or (caddr arg) +func+)
                                  :file (.. +func+ ".fish") }))
              (out (io/open (.. +func+ ".fish.nix") "w"))]
         (self out :write drv)
         (self out :close)
         (os/execute (format nil "nix-build {}.fish.nix -o {}"
                             +func+
                             (or (cadr arg)
                                 (.. +func+ ".out.fish"))))
         (os/remove (.. +func+ ".fish.nix")))])))
