function freeze
  set pkg (nix-build '<nixpkgs>' -A $argv[1] --no-out-link)
  nix-env -e $argv[1]
  nix-env -i $pkg
end
