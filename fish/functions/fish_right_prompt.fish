function fish_right_prompt
  if [ $status -eq 0 ]
    printf '%s:D ' (set_color $fish_color_happy)
  else
    printf '%sD: ' (set_color $fish_color_sad)
  end
  if test $CMD_DURATION
    printf '%s(%s) ' (set_color $fish_color_timing) (echo "$CMD_DURATION 1000" | awk '{printf "%.3fs", $1 / $2}')
  end

  set_color $fish_color_username
  printf '%s' (whoami)
  set_color normal
  printf ' at '

  set_color $fish_color_hostname
  echo -n (prompt_hostname | sed -re 's/-/ /')
  set_color normal
  printf ' in '

  set_color $fish_color_cwd
  printf '%s' (prompt_pwd)
  set_color normal
end
