function fish_mode_prompt
  switch $fish_bind_mode
    case default
      set_color '#6e5d9d'
      echo 'n '
    case insert
      set_color blue
      echo 'i '
    case visual
      set_color magenta
      echo 'v '
    case replace_one
      set_color red
      echo 'r '
  end
end

