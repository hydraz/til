function fish_prompt
  # used to be \u26A1
  printf '%s%% ' (set_color $fish_color_prompt blue)
  set_color normal
end
