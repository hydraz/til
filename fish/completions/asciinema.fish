function __fish_asciinema_subcommand
  set cmd (commandline -opc)
  if [ (count $cmd) -eq 1 -a $cmd[1] = 'asciinema' ]
    return 0
  end
  return 1
end

function __fish_asciinema_using_command
  set cmd (commandline -opc)
  if [ (count $cmd) -gt 1 ]
    if [ $argv[1] = $cmd[2] ]
      return 0
    end
  end
  return 1
end

complete -c asciinema -n '__fish_asciinema_subcommand' -a rec -d 'Record terminal session' -f
complete -c asciinema -n '__fish_asciinema_subcommand' -a play -d 'Replay terminal session' -f
complete -c asciinema -n '__fish_asciinema_subcommand' -a upload -d 'Upload locally saved terminal session to asciinema.org' -f
complete -c asciinema -n '__fish_asciinema_subcommand' -a auth -d 'Manage recordings on asciinema.org account'
complete -c asciinema -n '__fish_asciinema_subcommand' --no-files

complete -c asciinema -n '__fish_asciinema_using_command rec' -s t -o title \
  --no-files -d 'the title of the asciicast' -r
complete -c asciinema -n '__fish_asciinema_using_command rec' -s w -o max-wait \
  --no-files -d 'limit recorded terminal inactivity to maximum length' -r
complete -c asciinema -n '__fish_asciinema_using_command rec' -s y -o yes \
  --no-files -d 'answer "yes" to all prompts'
complete -c asciinema -n '__fish_asciinema_using_command rec' -s q -o quiet \
  --no-files -d 'be quiet, suppress all notices/warnings (implies -y)'

complete -c asciinema -n '__fish_asciinema_using_command play' -s w -o max-wait \
  --no-files -d 'limit recorded terminal inactivity to maximum length' -r
complete -c asciinema -n '__fish_asciinema_using_command play' -s s -o speed \
  --no-files -d 'playback speedup' -r
