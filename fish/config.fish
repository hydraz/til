set -x PATH $HOME/.local/bin \
            $HOME/.config/nvim/plugged/fzf/bin \
            $PATH

set -x EDITOR nvim

if [ -n "$name" ]
  set -x __fish_prompt_hostname $name
end

source ~/.config/fish/colors.fish

if [ -f ~/.config/fish/after_init.fish ]
  source ~/.config/fish/after_init.fish
end

setenv SSH_ENV $HOME/.ssh/environment

function start_agent
  ssh-agent -c > $SSH_ENV
  chmod 600 $SSH_ENV
  . $SSH_ENV > /dev/null
end

function idents
  ssh-add -l | grep "The agent has no identities" > /dev/null
  if [ $status -eq 0 ]
    ssh-add
    if [ $status -eq 2 ]
      start_agent
    end
  end
end

function :ge
  env TERM=dumb nvim-qt $argv
end

if [ -f $SSH_ENV ]
  . $SSH_ENV > /dev/null
  stat /proc/$SSH_AGENT_PID/cmdline >/dev/null 2>/dev/null
  if [ $status -ne 0 ]
    start_agent
  end
else
  start_agent
end

idents
