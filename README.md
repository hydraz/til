# til

til (portuguese for tilde) is where the dotfiles live.

## Sections

These dotfiles are structured in a tree for easier navigation. The outline of this structure is defined here:

- `X11`  
	Things which are related to the graphical environment, such as the Xresources files (which controls theming and settings for some applications, such as rofi and xft) and the XCompose file.  
	- `i3`  
		This contains configuration for the i3 window manager.
	- `bar`  
		Contains configuration files for lemonbar.
- `bin`  
	Scripts are placed here (in general, everything with a `x` bit).
	- `src`  
		Scripts that need to be compiled go here.
- `git`  
  Contains configuration files for git.
- `neovim`  
	Editor configuration for neovim.  
	* `autoload`  
		Automatically loaded scripts.
	* `ftplugin`  
		Scripts loaded for a filetype.
	* `plugin`  
		Vim plugin scripts.
	* `syntax`  
		Syntax highlighting files.
- `nix`  
  Configuration for NixOS and the Nix package manager.
	- `packages`  
		Custom package derivations.
- `zshrc.d`  
  Contains Z-shell configuration.
