#!/usr/bin/env bash

HIGHLIGHT="$(tput setaf 2)"
RESET="$(tput sgr0)"

prompt () {
  read -p "$@ [yN]" response
  case response in
    y|Y) return 0 ;;
    *) return 1 ;;
  esac
}

install-file () {
  mkdir -p $(dirname $2)
  if [[ -f $2 || -d $2 ]]; then
    if [[ $ASSUME_YES ]]; then
      rm -rf $2
    else
      read -p "$(tput setaf 3)$(basename $2)${RESET} exists, overwrite? [yaN] " response
      case $response in
        y|Y) rm -rf $2 ;;
        n|N) echo "SKIP  $2"; return ;;
        a|A) ASSUME_YES=yes; rm -rf $2 ;;
        * ) echo "SKIP  $2"; return ;;
      esac
    fi
  fi

  echo "${HIGHLIGHT}LN${RESET}    $2"
  ln -sf $PWD/$1 $2
}

install-dir () {
  for file in $1/*; do
    install-file $file $2
  done
}

install-group () {
  echo "${HIGHLIGHT}GROUP${RESET} $1"
  install-$1 ${TARGET:-$HOME}
}

install-zsh () {
  for file in zshrc zshrc.d/* zshrc.d/**/*; do
    [[ -f $file ]] && install-file $file $1/.$file
  done
}

install-X11 () {
  for file in X11/*; do
    [[ -f $file ]] && install-file $file $1/.$(basename $file)
  done
}

install-vim () {
  install-file neovim/init.vim $1/.config/nvim/init.vim
  for file in neovim/ftplugin/*; do
    [[ -f $file ]] && install-file $file \
      $1/.config/nvim/after/ftplugin/$(basename "$file")
  done

  for file in neovim/syntax/*; do
    [[ -f $file ]] && install-file $file \
      $1/.config/nvim/after/syntax/$(basename "$file")
  done

  for file in neovim/colours/*; do
    [[ -f $file ]] && install-file $file \
      $1/.config/nvim/colors/$(basename "$file")
  done
  for file in neovim/autoload/*; do
    [[ -f $file ]] && install-file $file \
      $1/.config/nvim/autoload/$(basename "$file")
  done
  for file in neovim/plugin/*; do
    [[ -f $file ]] && install-file $file \
      $1/.config/nvim/plugin/$(basename "$file")
  done
  if [[ ! -d ~/.config/nvim/plugged || ! -d ~/.config/nvim/autoload/plug.vim ]]; then
    echo 'Installing vim-plug and plugins is optional.'
    read -p "$(tput setaf 3)Install?${RESET} [yaN] " response
    case $response in
      y|Y) curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs \
                 https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
           nvim +PlugInstall +qall ;;
      n|N) echo "SKIP  vim-plug"; return ;;
    esac
  fi
}

install-i3 () {
  install-file X11/i3/compton.conf $1/.config/compton.conf
  install-file X11/i3/config $1/.i3/config
  install-file autostart $1/.autostart
  install-file autostart.sh $1/.autostart.sh
}

install-atom () {
  for file in atom/{init.coffee,keymap.cson,snippets.cson,styles.less}; do
    install-file $file $1/.atom/$(basename $file)
  done
  install-file atom/keylayout.json $1/.atom_keylayout.json
}

install-bin () {
  for file in bin/*; do
    [[ -f $file ]] && install-file $file $1/.local/bin/$(basename $file)
  done
}

install-bin-src () {
  for file in bin/src/*; do
    case `basename $file` in
      *.hs) ghc $file -outputdir /tmp -o $1/.local/bin/$(basename -s .hs $file)
            chmod 755 $1/.local/bin/$(basename -s .hs $file) ;;
      *.lisp) urn $file --emit-lua $1/.local/bin/$(basename -s .lisp $file) --chmod --shebang
              mv $1/.local/bin/$(basename -s .lisp $file).lua $1/.local/bin/$(basename -s .lisp $file) ;;
      *)      echo "don't know how to compile $file" ;;
    esac
  done
}

install-misc () {
  install-file bashrc $1/.bashrc
  install-file muttrc $1/.muttrc
  install-file git/gitconfig $1/.gitconfig
  install-file git/gitignore_global $1/.gitignore_global
}

install-nix () {
  install-file nix/config.nix $1/.config/nixpkgs/config.nix
  $SH nix/setup.sh "$1"
  echo 'Installing system-wide configuration.nix requires root priviledges.'
  read -p "$(tput setaf 3)Continue?${RESET} [yaN] " response
  if [[ $ASSUME_YES ]]; then
    sudo $0 nix-system-wide
  else
    case $response in
      y|Y)
        sudo $0 nix-system-wide ;;
      n|N) echo "SKIP  configuration.nix"; return ;;
    esac
  fi
}

install-nix-system-wide () {
  if [[ "$(id -u)" != "0" ]]; then
    echo 'You do not have permission to overwrite /etc/nixos/configuration.nix.'
    echo "Re-execute this program as root, using \`sudo $0\`"
    echo "  or answer [y] to the prompt given by \`$0 nix\`."
  else
    install-file nix/system.nix /etc/nixos/configuration.nix
  fi
}

install-X11-aux () {
  install-file X11/polybar_config $1/.config/polybar/config
  install-file X11/dunst_config $1/.config/dunst/dunstrc
}

install-fish () {
  for file in fish/*.fish; do
    [[ -f $file ]] && install-file $file $1/.config/fish/$(basename $file)
  done

  for file in fish/completions/*; do
    [[ -f $file ]] && install-file $file $1/.config/fish/completions/$(basename $file)
  done

  f=$(mktemp -u)
  nix_fish_deps=$f.lua
  urn fish/nix-fish-deps.lisp --emit-lua $f --shebang --chmod
  for file in fish/functions/*; do
    if grep "nix-fish-deps" $file &>/dev/null; then
      rm $1/.config/fish/functions/$(basename $file)
      $nix_fish_deps $PWD/fish/functions/$(basename -s .fish $file) \
        $1/.config/fish/functions/$(basename $file) \
        $(basename -s .fish $file) > /dev/null
    elif [[ -f $file ]]; then
      install-file $file $1/.config/fish/functions/$(basename $file)
    fi
  done
  rm $nix_fish_deps
}

if [[ $1 ]]; then
  for group in $@; do
    install-group $group
  done
else
  if [[ "$DISPLAY" != "" ]]; then
    install-group X11
    install-group i3
    install-group X11-aux
  fi
  if echo $SHELL | grep fish &>/dev/null; then
    install-group bin-src
    install-group fish
  else
    install-group zsh
  fi
  install-group vim
  install-group bin
  install-group misc
  if which nix-env &>/dev/null; then
    install-group nix
  fi
fi
