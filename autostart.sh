#!/usr/bin/bash

while read -r line; do
  prog=$(echo $line | cut -f1)
  if pgrep $prog &>/dev/null; then
    echo "Skipping $prog"
  else
    echo "Starting $prog"
    nohup sh -c "$line" &>/dev/null &
  fi
done < $HOME/.autostart
