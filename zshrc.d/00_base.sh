unset MANPATH
export HISTFILE=~/.zhistory
export HISTSIZE=1000000000000
export SAVEHIST=1000000000000

setopt appendhistory \
  autocd \
  beep \
  extendedglob \
  correct \
  prompt_subst \
  interactivecomments

bindkey -e

autoload -U colors
colors

export fpath=($HOME/.zshrc.d/zsh-completions $fpath)
export REPORTTIME=10
