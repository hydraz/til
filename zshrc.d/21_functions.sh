## Dotfile management ##

## Vim-like thingies ##

:w () {
  if git status &>/dev/null; then
    if [[ $1 != "" ]]; then
      git add $1
      git commit $1
    else
      git add -A
      git commit -a
    fi
  else
    sync
  fi
}

:e () {
  eval "$EDITOR $@"
}

## Directory navigation ##

u () {
  for i in $(seq 1 ${1:-1}); do
    cd ..
  done
}

.. () {
  u $@
}

back () {
  cd - &>/dev/null
}

mkcd () {
  mkdir -p $1
  cd $1
}

## Package management ##

pacsize () {
  for package in $@; do
    echo "$package: `/usr/bin/pacman -Qi $package | grep Installed\ Size | sed -re "s|.*: (.*)|\1|"`"
  done
}

## Genernal ##
async () {
  nohup sh -c "$@" &>/dev/null &
  if [[ -f nohup.out ]]; then
    rm -rf nohup.out
  fi
}

ds3-led () {
  DS3_CLASS=$(ls /sys/class/leds/*::sony* | head -n 1 | sed -re "s|^/sys/class/leds/(.*)::sony1:|\1|")
  led=/sys/class/leds/$DS3_CLASS::sony$1/brightness
  echo $2 | sudo tee $led > /dev/null
}


crypt () {
  case $1 in
    *.enc) openssl enc -d -a -in $1 -out ${2:-`echo $1 | sed -re "s/^(.*)\.enc$/\1/"`} -aes-256-cbc ;;
    *)     openssl enc -e -a -in $1 -out ${2:-$1.enc} -aes-256-cbc ;;
  esac
}

wd () {
  . ~/.local/bin/wd/wd.sh
}

for fun in ~/.zshrc.d/huge-functions/*; do
  if ! which $fun &>/dev/null; then
    fun_name="$(basename $fun | sed -re "s|\.sh$||g")"
    eval "$fun_name () {
      load_it () {
        source $fun
      }
      load_it
      eval \"$fun_name\" \"\$@\"
    }"
    # In case you couldn't fucking tell,
    # HACK HACK HACK HACK.
    # Evaluates a string containing a function that compiles a huge-function at
    # runtime, to reduce startup times.
  fi
done
