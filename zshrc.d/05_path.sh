np=$HOME/.local/bin:
np+=$HOME/.cabal/bin:
np+=$HOME/.cargo/bin:
np+=$HOME/.multirust/toolchains/nightly/cargo/bin:
np+=$HOME/.luarocks/bin:

export PATH="$np:$PATH"
