alias \
  fuck='sudo $(fc -ln -1)' \
  ls="ls --color=auto" \
  grep="grep --color=auto" \
  less="less -R" \
  :q="exit" \
  :wq=":w && :q" \
  cc="${CC} ${PREFER_C:--std=c11} $CFLAGS" \
  c++="${CXX} ${PREFER_CXX:--std=c++11} $CXXFLAGS" \
  make="make $MAKEFLAGS" \
  remake="make $MAKEFLAGS -B" \
  makemake="remake" \
  ll="ls -la" \
  wget="curl -O" \
  reload="exec zsh"
