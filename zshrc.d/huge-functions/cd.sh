cd () {
  export LASTPATH=$PWD
  _sed_string="s|(\w)/(\w)|\1_\2|g;s|^/||g;s|(\w)-(\w)|\1dash\2|g;s|(\w) (\w)|\1spc\2|g;s|/\.?$||g;s|\.\.|bck|g;"
  _sed_string+="s|\/|slsh|g;s|\.|dot|g"
  canonical () {
    if echo "$1" | grep '^/' &>/dev/null; then
      _ret="$1"
    else
      _ret="$PWD/$1"
    fi
    # if [[ -L $_ret ]]; then
    #   _ret=$(readlink $_ret) # readlink if it's a link
    # fi
    ret=$(echo "$_ret" | sed -re $_sed_string)
    echo "$_ret"
    unset "$ret"
  }

  prepare_env () {
    import () {
      for _thing in $@; do
        source "$_path/$_thing"
      done
    }

    export ZSHLC_PATH=$_path
  }

  cleanup_env () {
    unset -f import
  }
  _path="$(canonical "$1")"

  if [[ $1 == '-' ]]; then
    popd &>/dev/null;
  else
    if [[ -d "$_path" ]]; then
      prepare_env

      var="__sourced_$(echo $_path | sed -re $_sed_string)"
      if [[ ${(P)var} != "sourced" ]]; then
        if [[ -f "$_path/.zshlc" ]]; then
          source "$_path/.zshlc"
          export "__sourced_$(echo $_path | sed -re $_sed_string)"=sourced
        elif [[ -d "$_path/.zshlc.d" ]]; then
          for file in "$_path/.zshlc.d/*"; do
            [[ -f "$file" ]] && source "$file"
          done
          export "__sourced_$(echo "$_path" | sed -re $_sed_string)"=sourced
        fi
      fi

      cleanup_env
      pushd "$_path" &>/dev/null
    else
      echo "bcd: no such file or directory: "$1""
    fi
  fi
}
