autoload -Uz compinit
compinit

zstyle ':completion:*:default'         list-colors ":di=0;35::di=0;35:ln=1;96::di=1;35:ln=1;96::di=1;35:ln=1;96:"
zstyle ':completion:*' menu select
zstyle ':completion:*:descriptions'    format $'%{\e[0;31m%}completing %B%d%b%{\e[0m%}'
zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate

compdef _gnu_generic rustc
compdef _gnu_generic atom
compdef _gnu_generic ld
