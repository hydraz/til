export \
  GCC_COLORS="error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01;34:quote=01" \
  MAKEFLAGS="-j`nproc`" \
  VISUAL="nvim" \
  EDITOR="nvim" \
  LS_COLORS=":di=0;35::di=0;35:ln=1;96::di=0;35:ln=1;96::di=0;35:ln=1;96:" \
  TERMINAL="urxvt" \
  CFLAGS="-fdiagnostics-color=auto -g -O3 -Wall  -Wextra  -pipe -march=$(march) -mtune=$(march)" \
  CXXFLAGS="-fdiagnostics-color=auto -g -O3 -Wall -Wextra -pipe -march=$(march) -mtune=$(march)" \
  LD=ld.gold \
  PREFER_CXX="-std=c++1z" \
  PREFER_C="-std=c11" \
  LESS="-R" \
  LESSOPEN="|pygmentize -g -f 256 %s" \
  CC="ccache clang" \
  CXX="ccache clang++"


# MAN COLORS #

default=$(tput sgr0)
red=$(tput setaf 1)
green=$(tput setaf 2)
purple=$(tput setaf 5)
orange=$(tput setaf 9)

export LESS_TERMCAP_mb=$red
export LESS_TERMCAP_md=$orange
export LESS_TERMCAP_me=$default
export LESS_TERMCAP_se=$default
export LESS_TERMCAP_so=$purple
export LESS_TERMCAP_ue=$default
export LESS_TERMCAP_us=$green

unset GNOME_DESKTOP_SESSION_ID

export NPM_PACKAGES="$HOME/.npm-packages"
export NODE_PATH="$NPM_PACKAGES/lib/node_modules${NODE_PATH:+:$NODE_PATH}"

export RUST_SRC_PATH=$HOME/.important/rustc-nightly/src
export MYVIMRC=~/.config/nvim/init.vim
export VIMDIR=~/.config/nvim

# case $TERM in
#   xterm) export DO_GIT_PROMPT=no ;;
#   *) ;;
# esac

if [[ "$name" != "" ]]; then
  export HOST="${HOST}[${name}]"
  unset name
fi

export URN_COLOURS='(mono 1)'
