haskprompt="%{\x1b[38;2;124;105;178m%}<%{\x1b[38;2;224;123;217m%}$%{\x1b[38;2;124;105;178m%}>%{\x1b[0m%}"
hask_etcprmpt="%{\x1b[38;2;124;105;178m%}<%{\x1b[38;2;224;123;217m%}*%{\x1b[38;2;124;105;178m%}>%{\x1b[0m%}"
lightning=$(echo -e "%{$fg_no_bold[yellow]%}\u26A1%{$reset_color%}")

face () {
  if [[ $1 == 0 ]]; then
    echo -n ":)"
  else
    echo -n ":("
  fi
}

build-prompt () {
  echo -n "%{$fg_no_bold[green]%}%n%{$reset_color%} "
  echo -n "%{$fg_no_bold[red]%}%m%{$reset_color%} "
  echo -n "%{$fg_no_bold[yellow]%}${1:-$PROMPTCHAR}%{$fg_no_bold[white]%} "
}

precmd () {
  echo -ne "\033]0;${USER} @ ${PWD}\007"
}

PROMPTCHAR="\u26A1"

lightningfmt=$(echo -e "%%{$fg_no_bold[yellow]%%}$PROMPTCHAR%%{$reset_color%%}")

fmt=" $lightningfmt %%{$fg_no_bold[green]%%}%s $lightningfmt %%{$fg_no_bold[blue]%%}%d%%{$fg_no_bold[green]%%}%d%%{$fg_no_bold[red]%%}%d"

export PROMPT='$(build-prompt)'
export RPROMPT='%(?..%{$fg_no_bold[red]%}[%?] %})%{$fg_no_bold[green]%}%3~%{$fg_no_bold[white]%}$(rprompt $fmt)%{$reset_color%}'

export PROMPT2='$(build-prompt "%{$fg_no_bold[yellow]%}…%{$reset_color%}")'
export RPROMPT2='%{$fg_no_bold[red]%}%1_%{$fg_no_bold[white]%}'
